package store.impl;

import store.api.WordStore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Реализация хранилища
 */
public class WordStoreImpl implements WordStore {

    private final Map<String, Set<String>> wordsStore = new HashMap<>();


    @Override
    public synchronized void addWords(String word, String[] values) {
        if (wordsStore.containsKey(word)) {
            for (String value : values) {
                wordsStore.get(word).add(value);
            }
        } else {
            wordsStore.put(word, new HashSet<>(Arrays.asList(values).subList(0, values.length)));
        }
    }

    @Override
    public synchronized String[] getWordsByKey(String word) {
        String[] values = null;
        if (wordsStore.containsKey(word)) {
            Iterator<String> iterator = wordsStore.get(word).iterator();
            int size = wordsStore.get(word).size();
            values = new String[size];
            while (iterator.hasNext()) {
                //сделано для того чтобы массив значений заполнялся в таком же порядке в каком приходил при добавлении
                values[size - 1] = iterator.next();
                size--;
            }
        }
        return values;
    }

    @Override
    public synchronized Map<String, Boolean> deleteWords(String word, String[] values) {

        Map<String, Boolean> deletedValues = new HashMap<>();

        if (wordsStore.containsKey(word)) {
            for (String value : values) {
                   deletedValues.put(value, wordsStore.get(word).remove(value));
            }
        }
        return deletedValues;
    }
}
