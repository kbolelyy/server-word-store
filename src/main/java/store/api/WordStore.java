package store.api;

import java.util.Map;

/**
 * Интерфейс хранилища данных
 */
public interface WordStore {

    /**
     * Добавляет слово в справочник с заданными значениями
     *
     * @param word   ключевое слово
     * @param values значения которые относятся к слову.
     */
    void addWords(String word, String[] values);

    /**
     * Получает все значения относящиеся к заданному слову
     *
     * @param word ключевое слово
     * @return массив значений
     */
    String[] getWordsByKey(String word);


    /**
     * Удаляет значения слова из справочника
     *
     * @param word   ключевое слово
     * @param values значения которые должны быть удалены
     * @return мапа удаленных значений где значения успешно и не успешно
     */
    Map<String, Boolean> deleteWords(String word, String[] values);


}
