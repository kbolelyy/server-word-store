

import store.api.WordStore;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;

/**
 * Объект соединения клиента с сервером
 * который отвечает за выполнение операций
 */
public class ClientConnection extends Thread {

    private Socket clientSocket;
    private ObjectInputStream ois;
    private ObjectOutputStream ous;
    private WordStore wordStore;

    ClientConnection(Socket clientSocket, WordStore wordStore) throws IOException {
        this.clientSocket = clientSocket;
        this.ous = new ObjectOutputStream(clientSocket.getOutputStream());
        this.ois = new ObjectInputStream(clientSocket.getInputStream());
        this.wordStore = wordStore;

        start();
    }

    @Override
    public void run() {

        MessageObject message;
        try {
            message = (MessageObject) ois.readObject();
            System.out.println("client send request: " + message.toString());
            ous.writeObject(runClientCommand(message.getCommand(), message.getKeyWord(), message.getWords()));
            ous.flush();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                ous.close();
                ois.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Выполнение команды клиента
     *
     * @param command команда
     * @param word    ключевое слово
     * @param words   значения
     * @return ответ в виде строки
     */
    private String runClientCommand(String command, String word, String[] words) {

        StringBuilder stringBuilder = new StringBuilder();
        switch (command) {
            case "add":
                wordStore.addWords(word, words);
                stringBuilder.append("words was added successfully!");
                break;
            case "get":
                fillGetResponse(stringBuilder, wordStore.getWordsByKey(word));
                break;
            case "delete":
                fillDeleteResponse(stringBuilder, word, wordStore.deleteWords(word, words));
                break;
            default:
                stringBuilder.append(String.format("command not supported: %s", command));
        }
        return stringBuilder.toString();
    }


    /**
     * ответ для клиента команды delete
     */
    private void fillDeleteResponse(StringBuilder stringBuilder, String word, Map<String, Boolean> deletedValues) {
        if (deletedValues == null || deletedValues.size() == 0) {
            stringBuilder
                    .append("can`t delete words, because words not exist in store.");
        } else {
            for (Map.Entry<String, Boolean> deleted : deletedValues.entrySet()) {
                if (deleted.getValue()) {
                    stringBuilder.append(deleted.getKey())
                            .append(" was deleted successfully from key word: ")
                            .append(word)
                            .append(System.lineSeparator());
                } else {
                    stringBuilder.append(deleted.getKey())
                            .append(" can`t be deleted because not exist.")
                            .append(System.lineSeparator());
                }
            }
        }
    }


    /**
     * ответ для клиента команды get
     */
    private void fillGetResponse(StringBuilder stringBuilder, String[] values) {
        if (values == null || values.length == 0) {
            stringBuilder
                    .append(("can`t get words, because words not exist in store."));
        } else {
            for (String value : values) {
                stringBuilder.append(value)
                        .append(System.lineSeparator());
            }
        }
    }


}


