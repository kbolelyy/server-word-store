import store.api.WordStore;
import store.impl.WordStoreImpl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


/**
 * Класс запуска сервера
 */
public class Server {


    public static void main(String[] args) {

        ServerSocket serverSocket;
        Socket clientSocket;
        try {
            serverSocket = new ServerSocket(8080);
            System.out.println("Server started on address: " + serverSocket.getLocalSocketAddress());

            WordStore wordStore = new WordStoreImpl();

            while (true) {
                clientSocket = serverSocket.accept();
                System.out.format("Client with address: %s connect to server. %n", clientSocket.getRemoteSocketAddress());
                new ClientConnection(clientSocket, wordStore);
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }

    }
}
