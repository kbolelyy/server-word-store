import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import store.api.WordStore;
import store.impl.WordStoreImpl;

import java.util.Map;

public class WordStoreTest {

    private String[] values = null;
    private WordStore wordStore = null;

    @Before
    public void initWordStore() {
        values = new String[]{"Привет", "Здрасти", "Шалом"};
        wordStore = new WordStoreImpl();
    }

    @Test
    public void wordStoreAdd() {
        wordStore.addWords("hello", values);
        Assert.assertArrayEquals(values, wordStore.getWordsByKey("hello"));
    }

    @Test
    public void wordStoreDelete() {
        wordStore.addWords("hello", values);

        Map<String, Boolean> hello = wordStore.deleteWords("hello", new String[]{"Здрасти", "Asd"});
        int size = wordStore.getWordsByKey("hello").length;
        Assert.assertEquals(size, 2);

        Assert.assertEquals(true ,hello.get("Здрасти"));
        Assert.assertEquals(false ,hello.get("Asd"));
    }
}
